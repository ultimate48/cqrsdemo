﻿using CustomerApi.Commands;
using CustomerApi.Models.SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApi.Services
{
    public interface ICustomerWriteService
    {
        int Create(CreateCustomerCommand customer);
        int Delete(long id);
        int Update(long id, UpdateCustomerCommand customer);
    }

    public class CustomerWriteService : ICustomerWriteService
    {
        private readonly ICommandHandler<Command> _commandHandler;
        private readonly CustomerSQLiteRepository _sqliteRepository;
        public CustomerWriteService(ICommandHandler<Command> commandHandler, CustomerSQLiteRepository sqliteRepository)
        {
            _commandHandler = commandHandler;
            _sqliteRepository = sqliteRepository;
        }
        public int Create(CreateCustomerCommand customer)
        {
            try
            {
                _commandHandler.Execute(customer);
                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public int Update(long id, UpdateCustomerCommand customer)
        {
            var record = _sqliteRepository.GetById(id);
            if (record == null)
            {
                return 0;
            }
            try
            {
                customer.Id = id;
                _commandHandler.Execute(customer);

                return 1;
            }
            catch
            {
                return 0;
            }
        }

        public int Delete(long id)
        {
            var record = _sqliteRepository.GetById(id);
            if (record == null)
            {
                return 0;
            }
            try
            {
                _commandHandler.Execute(new DeleteCustomerCommand()
                {
                    Id = id
                });

                return 1;
            }
            catch
            {
                return 0;
            }
        }
    }
}
