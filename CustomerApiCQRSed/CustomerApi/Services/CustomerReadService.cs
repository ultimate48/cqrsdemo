﻿using CustomerApi.Models.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerApi.Services
{
    public interface ICustomerReadService
    {
        List<CustomerEntity> GetAllCustomers();
        CustomerEntity GetByEmail(string email);
        CustomerEntity GetById(long id);
    }

    public class CustomerReadService : ICustomerReadService
    {
        private readonly CustomerMongoRepository _mongoRepository;
        public CustomerReadService(CustomerMongoRepository repository)
        {
            _mongoRepository = repository;
        }

        public List<CustomerEntity> GetAllCustomers()
        {
            return _mongoRepository.GetCustomers();
        }

        public CustomerEntity GetById(long id)
        {
            return _mongoRepository.GetCustomer(id);
        }

        public CustomerEntity GetByEmail(string email)
        {
            return _mongoRepository.GetCustomerByEmail(email);
        }
    }
}
