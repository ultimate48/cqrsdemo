﻿using CustomerApi.Commands;
using CustomerApi.Models;
using CustomerApi.Models.Mongo;
using CustomerApi.Models.SQLite;
using CustomerApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace CustomerApi.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        private readonly ICustomerReadService _customerReadService;
        private readonly ICustomerWriteService _customerWriteService;

        public CustomersController(ICustomerReadService customerReadService, ICustomerWriteService customerWriteService)
        {
            _customerReadService = customerReadService;
            _customerWriteService = customerWriteService;
        }

        [HttpGet]
        public List<CustomerEntity> Get()
        {
            return _customerReadService.GetAllCustomers();
        }

        [HttpGet("{id}", Name = "GetCustomer")]
        public IActionResult GetById(long id)
        {
            var customer = _customerReadService.GetById(id);
            if (customer == null)
            {
                return NotFound();
            }

            return new ObjectResult(customer);
        }

        [HttpGet("{email}")]
        public IActionResult GetByEmail(string email)
        {
            var customer = _customerReadService.GetByEmail(email);
            if (customer == null)
            {
                return NotFound();
            }

            return new ObjectResult(customer);
        }

        [HttpPost]
        public IActionResult Post([FromBody] CreateCustomerCommand customer)
        {
            try
            {
                _customerWriteService.Create(customer);

                return CreatedAtRoute("GetCustomer", new { id = customer.Id }, customer);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] UpdateCustomerCommand customer)
        {
            try
            {
                _customerWriteService.Update(id, customer);
                return NoContent();
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            try
            {
                _customerWriteService.Delete(id);
                return NoContent();
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
